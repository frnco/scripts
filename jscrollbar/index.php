<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Pluggin JQuery : jScrollbar</title>
	<meta http-equiv="description" content="" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="jquery/jScrollbar.jquery.css" type="text/css" />
	
	<style type="text/css">
		body {
			margin:0px;
		}
		
		.jScrollbar, 
		.jScrollbar2,
		.jScrollbar3,
		.jScrollbar4,
		.jScrollbar5
		{float:left;margin:10px}
	</style>
</head>
<body>

<!-- EXEMPLE 1 -->
<div class="jScrollbar">
	<div class="jScrollbar_mask">
		<script src="http://widgets.twimg.com/j/2/widget.js"></script>
				<script>
                new TWTR.Widget({
                  version: 2,
                  type: 'profile',
                  rpp: 4,
                  interval: 30000,
                  width: 485,
                  height: 80,
                  theme: {
                    shell: {
                      background: 'none',
                      color: 'none'
                    },
                    tweets: {
                      background: 'none',
                      color: '#FFF',
                      links: '#FFF'
                    }
                  },
                  features: {
                    scrollbar: true,
                    loop: false,
                    live: false,
                    hashtags: true,
                    timestamp: false,
                    avatars: true,
                    behavior: 'all'
                  }
                }).render().setUser('groupsoftware').start();
                </script>
	</div>
	
	<div class="jScrollbar_draggable">
		<a href="#" class="draggable"></a>
	</div>
		
	<div class="clr"></div>
</div>


<script type="text/javascript" src="jquery/jquery.js"></script>           
<script type="text/javascript" src="jquery/jquery-ui.js"></script>        
<script type="text/javascript" src="jquery/jquery-mousewheel.js"></script>
<script type="text/javascript" src="jquery/jScrollbar.jquery.js"></script>
<script type="text/javascript">                                           
	$(document).ready(function(){
		$('.jScrollbar, .jScrollbar2, .jScrollbar3, .jScrollbar4, .jScrollbar5').jScrollbar();
	});                                        
</script>                                                                 
</body>                                                                   
</html>